# To run the application with Docker
## Build the api
```
./mvnw clean package
```
## Build the docker
Replace VERSION by the wanted docker image version.
```
docker build -t shortener/url:VERSION .
```
## Replace the docker image version
Go to the stack.yml file and replace the image version of api service.
## Run docker compose
### run
Go to the docker folder
```
docker-compose -f stack.yml up
```
### volume
The data folder is a volume for the database service, is useful to keep the data after a restart.  
## Wait the up
Wait than Database and api are up.
# Use the api
## Create un short URL
### creation
The Api consume json format and the structure is just a url key and the value is the url himself.
```
curl --location --request POST 'localhost:8787/shortener' \
--header 'Content-Type: application/json' \
--data-raw '{
    "url":"https://www.google.com/"
}'
```
### response
If all goo you will receive a json body with the same structure of the creation.  
The url will be the short url with his key.  
If the application reach his maximum key generation will receive a HTTP error 417.  
The number of possibility is :  
85 + 85^2 + 84^3 + 84^4 + 85^5 + 85^6 + 85^7 + 85^8 + 85^9 + 85^10 = 1.9921815e+19  
I take ASCCI value between 33 and 126 without 7 character (",',#,%,/,?,\,^,|)  
( 126 - 32 ) - 9 = 85  
If the url in the create request are not a valid url you will receive a HTTP error 400.  
## Get the url
To have the url you just have to use the url in the response body, this one is a redirection.  