package com.shortener.url.service;

import com.shortener.url.UrlApplicationTests;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


class UrlValidatorServiceTest extends UrlApplicationTests {


    @Test
    void testValid() {
        UrlValidator service = new UrlValidator();
        boolean valid = service.isValid("https://google.com");
        boolean notValid = service.isValid("qqq.google.comcom");
        assertTrue(valid);
        assertFalse(notValid);
    }
}
