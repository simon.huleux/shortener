package com.shortener.url.service;

import com.shortener.url.UrlApplicationTests;
import com.shortener.url.exception.OutOfIndex;
import com.shortener.url.model.ShortenerData;
import com.shortener.url.model.ShortenerID;
import com.shortener.url.repository.ShortenerDataRepository;
import com.shortener.url.repository.ShortenerIDRepository;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;


class ShortenerServiceTest extends UrlApplicationTests {


    @Test
    void getByKeyTestEmpty() {
        ShortenerDataRepository shortenerDataRepositoryMock = Mockito.mock(ShortenerDataRepository.class);
        ShortenerIDRepository shortenerIDRepositoryMock = Mockito.mock(ShortenerIDRepository.class);
        ShortenerService service = new ShortenerService(shortenerDataRepositoryMock, shortenerIDRepositoryMock);

        Mockito.when(shortenerDataRepositoryMock.findByKey(anyString())).then(invocationOnMock -> Optional.empty());

        Optional<ShortenerData> res = service.getByKey("testKey");
        assertFalse(res.isPresent());
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);

        verify(shortenerDataRepositoryMock, times(1)).findByKey(argumentCaptor.capture());
        assertEquals("Call with the wrong key", "testKey", argumentCaptor.getValue());
        verifyNoMoreInteractions(shortenerDataRepositoryMock);
        verifyNoMoreInteractions(shortenerIDRepositoryMock);
    }

    @Test
    void getByKeyTest() {
        ShortenerDataRepository shortenerDataRepositoryMock = Mockito.mock(ShortenerDataRepository.class);
        ShortenerIDRepository shortenerIDRepositoryMock = Mockito.mock(ShortenerIDRepository.class);
        ShortenerService service = new ShortenerService(shortenerDataRepositoryMock, shortenerIDRepositoryMock);

        ShortenerData mockData = new ShortenerData();

        Mockito.when(shortenerDataRepositoryMock.findByKey(anyString())).then(invocationOnMock -> Optional.of(mockData));

        Optional<ShortenerData> res = service.getByKey("testKey");
        assertTrue(res.isPresent());
        assertEquals(mockData, res.get());
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);

        verify(shortenerDataRepositoryMock, times(1)).findByKey(argumentCaptor.capture());
        assertEquals("Call with the wrong key", "testKey", argumentCaptor.getValue());
        verifyNoMoreInteractions(shortenerDataRepositoryMock);
        verifyNoMoreInteractions(shortenerIDRepositoryMock);
    }

    @Test
    void saveExistingTest() throws OutOfIndex {
        ShortenerDataRepository shortenerDataRepositoryMock = Mockito.mock(ShortenerDataRepository.class);
        ShortenerIDRepository shortenerIDRepositoryMock = Mockito.mock(ShortenerIDRepository.class);
        ShortenerService service = new ShortenerService(shortenerDataRepositoryMock, shortenerIDRepositoryMock);

        ShortenerData mockData = new ShortenerData();
        mockData.setKey("testKey");

        Mockito.when(shortenerDataRepositoryMock.findByUrl(anyString())).then(invocationOnMock -> Optional.of(mockData));

        String key = service.save("UrlKey");
        assertEquals("testKey", key);
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);

        verify(shortenerDataRepositoryMock, times(1)).findByUrl(argumentCaptor.capture());
        assertEquals("Call with the wrong key", "UrlKey", argumentCaptor.getValue());
        verifyNoMoreInteractions(shortenerDataRepositoryMock);
        verifyNoMoreInteractions(shortenerIDRepositoryMock);
    }

    @Test
    void saveFirstTest() throws OutOfIndex {
        ShortenerDataRepository shortenerDataRepositoryMock = Mockito.mock(ShortenerDataRepository.class);
        ShortenerIDRepository shortenerIDRepositoryMock = Mockito.mock(ShortenerIDRepository.class);
        ShortenerService service = new ShortenerService(shortenerDataRepositoryMock, shortenerIDRepositoryMock);


        Mockito.when(shortenerDataRepositoryMock.findByUrl(anyString())).then(invocationOnMock -> Optional.empty());
        Mockito.when(shortenerIDRepositoryMock.findAll()).then(invocationOnMock -> List.of());
        doReturn(null).when(shortenerDataRepositoryMock).save(any(ShortenerData.class));
        doReturn(null).when(shortenerIDRepositoryMock).save(any(ShortenerID.class));
        String key = service.save("UrlKey");
        assertEquals("!", key);
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);

        verify(shortenerDataRepositoryMock, times(1)).findByUrl(argumentCaptor.capture());
        assertEquals("Call with the wrong key", "UrlKey", argumentCaptor.getValue());
        verify(shortenerIDRepositoryMock, times(1)).findAll();
        ArgumentCaptor<ShortenerData> shortenerDataCaptor = ArgumentCaptor.forClass(ShortenerData.class);
        verify(shortenerDataRepositoryMock, times(1)).save(shortenerDataCaptor.capture());

        assertEquals("UrlKey", shortenerDataCaptor.getValue().getUrl());
        assertEquals("!", shortenerDataCaptor.getValue().getKey());

        ArgumentCaptor<ShortenerID> shortenerIDCaptor = ArgumentCaptor.forClass(ShortenerID.class);
        verify(shortenerIDRepositoryMock, times(1)).save(shortenerIDCaptor.capture());

        assertEquals("!", shortenerIDCaptor.getValue().getKey());
        verifyNoMoreInteractions(shortenerDataRepositoryMock);
        verifyNoMoreInteractions(shortenerIDRepositoryMock);
    }

    @Test
    void saveSecondTest() throws OutOfIndex {
        ShortenerDataRepository shortenerDataRepositoryMock = Mockito.mock(ShortenerDataRepository.class);
        ShortenerIDRepository shortenerIDRepositoryMock = Mockito.mock(ShortenerIDRepository.class);
        ShortenerService service = new ShortenerService(shortenerDataRepositoryMock, shortenerIDRepositoryMock);


        Mockito.when(shortenerDataRepositoryMock.findByUrl(anyString())).then(invocationOnMock -> Optional.empty());
        ShortenerID mockId = new ShortenerID();
        mockId.setKey("!");
        Mockito.when(shortenerIDRepositoryMock.findAll()).then(invocationOnMock -> List.of(mockId));
        doReturn(null).when(shortenerDataRepositoryMock).save(any(ShortenerData.class));
        doReturn(null).when(shortenerIDRepositoryMock).save(any(ShortenerID.class));
        String key = service.save("UrlKey");
        assertEquals("$", key);
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);

        verify(shortenerDataRepositoryMock, times(1)).findByUrl(argumentCaptor.capture());
        assertEquals("Call with the wrong key", "UrlKey", argumentCaptor.getValue());
        verify(shortenerIDRepositoryMock, times(1)).findAll();
        ArgumentCaptor<ShortenerData> shortenerDataCaptor = ArgumentCaptor.forClass(ShortenerData.class);
        verify(shortenerDataRepositoryMock, times(1)).save(shortenerDataCaptor.capture());

        assertEquals("UrlKey", shortenerDataCaptor.getValue().getUrl());
        assertEquals("$", shortenerDataCaptor.getValue().getKey());

        ArgumentCaptor<ShortenerID> shortenerIDCaptor = ArgumentCaptor.forClass(ShortenerID.class);
        verify(shortenerIDRepositoryMock, times(1)).save(shortenerIDCaptor.capture());

        assertEquals("$", shortenerIDCaptor.getValue().getKey());
        verifyNoMoreInteractions(shortenerDataRepositoryMock);
        verifyNoMoreInteractions(shortenerIDRepositoryMock);
    }

    @Test
    void saveFirstMaxValueFirstCharTest() throws OutOfIndex {
        ShortenerDataRepository shortenerDataRepositoryMock = Mockito.mock(ShortenerDataRepository.class);
        ShortenerIDRepository shortenerIDRepositoryMock = Mockito.mock(ShortenerIDRepository.class);
        ShortenerService service = new ShortenerService(shortenerDataRepositoryMock, shortenerIDRepositoryMock);


        Mockito.when(shortenerDataRepositoryMock.findByUrl(anyString())).then(invocationOnMock -> Optional.empty());
        ShortenerID mockId = new ShortenerID();
        mockId.setKey("~");
        Mockito.when(shortenerIDRepositoryMock.findAll()).then(invocationOnMock -> List.of(mockId));
        doReturn(null).when(shortenerDataRepositoryMock).save(any(ShortenerData.class));
        doReturn(null).when(shortenerIDRepositoryMock).save(any(ShortenerID.class));
        String key = service.save("UrlKey");
        assertEquals("!!", key);
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);

        verify(shortenerDataRepositoryMock, times(1)).findByUrl(argumentCaptor.capture());
        assertEquals("Call with the wrong key", "UrlKey", argumentCaptor.getValue());
        verify(shortenerIDRepositoryMock, times(1)).findAll();
        ArgumentCaptor<ShortenerData> shortenerDataCaptor = ArgumentCaptor.forClass(ShortenerData.class);
        verify(shortenerDataRepositoryMock, times(1)).save(shortenerDataCaptor.capture());

        assertEquals("UrlKey", shortenerDataCaptor.getValue().getUrl());
        assertEquals("!!", shortenerDataCaptor.getValue().getKey());

        ArgumentCaptor<ShortenerID> shortenerIDCaptor = ArgumentCaptor.forClass(ShortenerID.class);
        verify(shortenerIDRepositoryMock, times(1)).save(shortenerIDCaptor.capture());

        assertEquals("!!", shortenerIDCaptor.getValue().getKey());
        verifyNoMoreInteractions(shortenerDataRepositoryMock);
        verifyNoMoreInteractions(shortenerIDRepositoryMock);
    }

    @Test
    void saveNextValueTest() throws OutOfIndex {
        ShortenerDataRepository shortenerDataRepositoryMock = Mockito.mock(ShortenerDataRepository.class);
        ShortenerIDRepository shortenerIDRepositoryMock = Mockito.mock(ShortenerIDRepository.class);
        ShortenerService service = new ShortenerService(shortenerDataRepositoryMock, shortenerIDRepositoryMock);


        Mockito.when(shortenerDataRepositoryMock.findByUrl(anyString())).then(invocationOnMock -> Optional.empty());
        ShortenerID mockId = new ShortenerID();
        mockId.setKey("~~");
        Mockito.when(shortenerIDRepositoryMock.findAll()).then(invocationOnMock -> List.of(mockId));
        doReturn(null).when(shortenerDataRepositoryMock).save(any(ShortenerData.class));
        doReturn(null).when(shortenerIDRepositoryMock).save(any(ShortenerID.class));
        String key = service.save("UrlKey");
        assertEquals("!!!", key);
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);

        verify(shortenerDataRepositoryMock, times(1)).findByUrl(argumentCaptor.capture());
        assertEquals("Call with the wrong key", "UrlKey", argumentCaptor.getValue());
        verify(shortenerIDRepositoryMock, times(1)).findAll();
        ArgumentCaptor<ShortenerData> shortenerDataCaptor = ArgumentCaptor.forClass(ShortenerData.class);
        verify(shortenerDataRepositoryMock, times(1)).save(shortenerDataCaptor.capture());

        assertEquals("UrlKey", shortenerDataCaptor.getValue().getUrl());
        assertEquals("!!!", shortenerDataCaptor.getValue().getKey());

        ArgumentCaptor<ShortenerID> shortenerIDCaptor = ArgumentCaptor.forClass(ShortenerID.class);
        verify(shortenerIDRepositoryMock, times(1)).save(shortenerIDCaptor.capture());

        assertEquals("!!!", shortenerIDCaptor.getValue().getKey());
        verifyNoMoreInteractions(shortenerDataRepositoryMock);
        verifyNoMoreInteractions(shortenerIDRepositoryMock);
    }

    @Test
    void saveNextValue2Test() throws OutOfIndex {
        ShortenerDataRepository shortenerDataRepositoryMock = Mockito.mock(ShortenerDataRepository.class);
        ShortenerIDRepository shortenerIDRepositoryMock = Mockito.mock(ShortenerIDRepository.class);
        ShortenerService service = new ShortenerService(shortenerDataRepositoryMock, shortenerIDRepositoryMock);


        Mockito.when(shortenerDataRepositoryMock.findByUrl(anyString())).then(invocationOnMock -> Optional.empty());
        ShortenerID mockId = new ShortenerID();
        mockId.setKey("~!");
        Mockito.when(shortenerIDRepositoryMock.findAll()).then(invocationOnMock -> List.of(mockId));
        doReturn(null).when(shortenerDataRepositoryMock).save(any(ShortenerData.class));
        doReturn(null).when(shortenerIDRepositoryMock).save(any(ShortenerID.class));
        String key = service.save("UrlKey");
        assertEquals("!$", key);
        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);

        verify(shortenerDataRepositoryMock, times(1)).findByUrl(argumentCaptor.capture());
        assertEquals("Call with the wrong key", "UrlKey", argumentCaptor.getValue());
        verify(shortenerIDRepositoryMock, times(1)).findAll();
        ArgumentCaptor<ShortenerData> shortenerDataCaptor = ArgumentCaptor.forClass(ShortenerData.class);
        verify(shortenerDataRepositoryMock, times(1)).save(shortenerDataCaptor.capture());

        assertEquals("UrlKey", shortenerDataCaptor.getValue().getUrl());
        assertEquals("!$", shortenerDataCaptor.getValue().getKey());

        ArgumentCaptor<ShortenerID> shortenerIDCaptor = ArgumentCaptor.forClass(ShortenerID.class);
        verify(shortenerIDRepositoryMock, times(1)).save(shortenerIDCaptor.capture());

        assertEquals("!$", shortenerIDCaptor.getValue().getKey());
        verifyNoMoreInteractions(shortenerDataRepositoryMock);
        verifyNoMoreInteractions(shortenerIDRepositoryMock);
    }

    @Test
    void saveOutOfIndexTest() throws OutOfIndex {
        ShortenerDataRepository shortenerDataRepositoryMock = Mockito.mock(ShortenerDataRepository.class);
        ShortenerIDRepository shortenerIDRepositoryMock = Mockito.mock(ShortenerIDRepository.class);
        ShortenerService service = new ShortenerService(shortenerDataRepositoryMock, shortenerIDRepositoryMock);


        Mockito.when(shortenerDataRepositoryMock.findByUrl(anyString())).then(invocationOnMock -> Optional.empty());
        ShortenerID mockId = new ShortenerID();
        mockId.setKey("~~~~~~~~~~");
        Mockito.when(shortenerIDRepositoryMock.findAll()).then(invocationOnMock -> List.of(mockId));

        OutOfIndex ex = assertThrows(OutOfIndex.class, () -> service.save("UrlKey"));

        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);

        verify(shortenerDataRepositoryMock, times(1)).findByUrl(argumentCaptor.capture());
        assertEquals("Call with the wrong key", "UrlKey", argumentCaptor.getValue());
        verify(shortenerIDRepositoryMock, times(1)).findAll();
        verifyNoMoreInteractions(shortenerDataRepositoryMock);
        verifyNoMoreInteractions(shortenerIDRepositoryMock);
    }
}
