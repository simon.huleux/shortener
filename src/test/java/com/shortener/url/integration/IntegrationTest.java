package com.shortener.url.integration;

import com.shortener.url.UrlApplicationTests;
import com.shortener.url.model.ShortenerID;
import com.shortener.url.model.ShortenerRequest;
import com.shortener.url.repository.ShortenerIDRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IntegrationTest extends UrlApplicationTests {

    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private ShortenerIDRepository shortenerIDRepository;

    @Test
    public void notFoundCreateFound() {
        ResponseEntity<String> notFoundRes = restTemplate.getForEntity("/!", String.class);
        assertEquals(404, notFoundRes.getStatusCode().value());
        ShortenerRequest data = new ShortenerRequest();
        data.setUrl("https://google.com");

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");

        HttpEntity<ShortenerRequest> request = new HttpEntity<>(data, headers);
        ResponseEntity<ShortenerRequest> save = restTemplate.postForEntity("/shortener", request, ShortenerRequest.class);

        assertEquals(201, save.getStatusCode().value());
        assertEquals(restTemplate.getRootUri() + "/!", save.getBody().getUrl());

        ResponseEntity<String> found = restTemplate.getForEntity("/!", String.class);
        assertEquals(302, found.getStatusCode().value());
        assertEquals("google.com", found.getHeaders().getLocation().getHost());
        assertEquals("https", found.getHeaders().getLocation().getScheme());
    }

    @Test
    public void notUrl() {
        ShortenerRequest data = new ShortenerRequest();
        data.setUrl("notUrl");

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");

        HttpEntity<ShortenerRequest> request = new HttpEntity<>(data, headers);
        ResponseEntity<ShortenerRequest> notUrl = restTemplate.postForEntity("/shortener", request, ShortenerRequest.class);

        assertEquals(400, notUrl.getStatusCode().value());

    }

    @Test
    public void outIndex() {
        ShortenerID id = shortenerIDRepository.findAll().get(0);
        id.setKey("~~~~~~~~~~");
        this.shortenerIDRepository.save(id);

        ShortenerRequest data = new ShortenerRequest();
        data.setUrl("https://www.youtube.com/");

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");

        HttpEntity<ShortenerRequest> request = new HttpEntity<>(data, headers);
        ResponseEntity<ShortenerRequest> save = restTemplate.postForEntity("/shortener", request, ShortenerRequest.class);

        assertEquals(417, save.getStatusCode().value());
//        assertEquals(restTemplate.getRootUri() + "/!", save.getBody().getUrl());


    }


}
