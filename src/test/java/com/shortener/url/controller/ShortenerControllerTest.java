package com.shortener.url.controller;

import com.shortener.url.UrlApplicationTests;
import com.shortener.url.exception.OutOfIndex;
import com.shortener.url.model.ShortenerData;
import com.shortener.url.model.ShortenerRequest;
import com.shortener.url.service.ShortenerService;
import com.shortener.url.service.UrlValidator;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.view.RedirectView;

import java.io.IOException;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class ShortenerControllerTest extends UrlApplicationTests {

    @Test
    void noValidUrlTest() {
        UrlValidator urlValidatorMock = Mockito.mock(UrlValidator.class);
        ShortenerService shortenerServiceMock = Mockito.mock(ShortenerService.class);
        ShortenerController shortenerController = new ShortenerController(urlValidatorMock, shortenerServiceMock);
        when(urlValidatorMock.isValid(anyString())).then(invocationOnMock -> false);

        ShortenerRequest request = new ShortenerRequest();
        request.setUrl("test");
        ResponseStatusException ex = assertThrows(ResponseStatusException.class, () -> shortenerController.shortenUrl(request));
        assertEquals(400, ex.getRawStatusCode());
        assertEquals("Your request are not a valid url.", ex.getReason());

        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);

        verify(urlValidatorMock, times(1)).isValid(argumentCaptor.capture());
        assertEquals("test", argumentCaptor.getValue());

        verifyNoMoreInteractions(urlValidatorMock);
        verifyNoMoreInteractions(shortenerServiceMock);

    }

    @Test
    void validUrlButOutOfIndexTest() throws OutOfIndex {
        UrlValidator urlValidatorMock = Mockito.mock(UrlValidator.class);
        ShortenerService shortenerServiceMock = Mockito.mock(ShortenerService.class);
        ShortenerController shortenerController = new ShortenerController(urlValidatorMock, shortenerServiceMock);
        when(urlValidatorMock.isValid(anyString())).then(invocationOnMock -> true);
        when(shortenerServiceMock.save(anyString())).thenThrow(OutOfIndex.class);

        ShortenerRequest request = new ShortenerRequest();
        request.setUrl("test");
        ResponseStatusException ex = assertThrows(ResponseStatusException.class, () -> shortenerController.shortenUrl(request));
        assertEquals(417, ex.getRawStatusCode());
        assertEquals("We have an issue to process your request we are sorry.", ex.getReason());

        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);

        verify(urlValidatorMock, times(1)).isValid(argumentCaptor.capture());
        assertEquals("test", argumentCaptor.getValue());

        ArgumentCaptor<String> urlCaptor = ArgumentCaptor.forClass(String.class);

        verify(shortenerServiceMock, times(1)).save(urlCaptor.capture());
        assertEquals("test", urlCaptor.getValue());

        verifyNoMoreInteractions(urlValidatorMock);
        verifyNoMoreInteractions(shortenerServiceMock);

    }

    @Test
    void validUrlTest() throws OutOfIndex, IOException {
        UrlValidator urlValidatorMock = Mockito.mock(UrlValidator.class);
        ShortenerService shortenerServiceMock = Mockito.mock(ShortenerService.class);
        ShortenerController shortenerController = new ShortenerController(urlValidatorMock, shortenerServiceMock);
        when(urlValidatorMock.isValid(anyString())).then(invocationOnMock -> true);
        when(shortenerServiceMock.save(anyString())).then(invocationOnMock -> "key");

        ShortenerRequest request = new ShortenerRequest();
        request.setUrl("test");
        ResponseEntity<ShortenerRequest> response = shortenerController.shortenUrl(request);
        assertEquals("http://localhost/key", response.getBody().getUrl());

        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);

        verify(urlValidatorMock, times(1)).isValid(argumentCaptor.capture());
        assertEquals("test", argumentCaptor.getValue());

        ArgumentCaptor<String> urlCaptor = ArgumentCaptor.forClass(String.class);

        verify(shortenerServiceMock, times(1)).save(urlCaptor.capture());
        assertEquals("test", urlCaptor.getValue());

        verifyNoMoreInteractions(urlValidatorMock);
        verifyNoMoreInteractions(shortenerServiceMock);

    }


    @Test
    void validUrlWithPortAndHttpsTest() throws OutOfIndex, IOException {

        MockHttpServletRequest r = new MockHttpServletRequest();

        r.setRemoteHost("localhost");
        r.setScheme("https");
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(r));


        UrlValidator urlValidatorMock = Mockito.mock(UrlValidator.class);
        ShortenerService shortenerServiceMock = Mockito.mock(ShortenerService.class);
        ShortenerController shortenerController = new ShortenerController(urlValidatorMock, shortenerServiceMock);
        when(urlValidatorMock.isValid(anyString())).then(invocationOnMock -> true);
        when(shortenerServiceMock.save(anyString())).then(invocationOnMock -> "key");

        ShortenerRequest request = new ShortenerRequest();
        request.setUrl("test");
        ResponseEntity<ShortenerRequest> response = shortenerController.shortenUrl(request);
        assertEquals("https://localhost:80/key", response.getBody().getUrl());

        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);

        verify(urlValidatorMock, times(1)).isValid(argumentCaptor.capture());
        assertEquals("test", argumentCaptor.getValue());

        ArgumentCaptor<String> urlCaptor = ArgumentCaptor.forClass(String.class);

        verify(shortenerServiceMock, times(1)).save(urlCaptor.capture());
        assertEquals("test", urlCaptor.getValue());

        verifyNoMoreInteractions(urlValidatorMock);
        verifyNoMoreInteractions(shortenerServiceMock);

    }


    @Test
    void notFoundKeyTest() {
        UrlValidator urlValidatorMock = Mockito.mock(UrlValidator.class);
        ShortenerService shortenerServiceMock = Mockito.mock(ShortenerService.class);
        ShortenerController shortenerController = new ShortenerController(urlValidatorMock, shortenerServiceMock);
        when(shortenerServiceMock.getByKey(anyString())).then(invocationOnMock -> Optional.empty());

        ResponseStatusException ex = assertThrows(ResponseStatusException.class, () -> shortenerController.getKey("key"));
        assertEquals(404, ex.getRawStatusCode());
        assertEquals("Unable to find by this key : key", ex.getReason());

        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);

        verify(shortenerServiceMock, times(1)).getByKey(argumentCaptor.capture());
        assertEquals("key", argumentCaptor.getValue());

        verifyNoMoreInteractions(urlValidatorMock);
        verifyNoMoreInteractions(shortenerServiceMock);

    }

    @Test
    void foundKeyTest() throws Exception {
        UrlValidator urlValidatorMock = Mockito.mock(UrlValidator.class);
        ShortenerService shortenerServiceMock = Mockito.mock(ShortenerService.class);
        ShortenerController shortenerController = new ShortenerController(urlValidatorMock, shortenerServiceMock);
        ShortenerData data = new ShortenerData();
        data.setKey("key");
        data.setUrl("url");
        when(shortenerServiceMock.getByKey(anyString())).then(invocationOnMock -> Optional.of(data));

        RedirectView redirectView = shortenerController.getKey("key");
        assertEquals("url", redirectView.getUrl());

        ArgumentCaptor<String> argumentCaptor = ArgumentCaptor.forClass(String.class);

        verify(shortenerServiceMock, times(1)).getByKey(argumentCaptor.capture());
        assertEquals("key", argumentCaptor.getValue());

        verifyNoMoreInteractions(urlValidatorMock);
        verifyNoMoreInteractions(shortenerServiceMock);

    }
}
