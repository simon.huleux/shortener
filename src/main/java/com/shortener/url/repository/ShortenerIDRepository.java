package com.shortener.url.repository;

import com.shortener.url.model.ShortenerID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShortenerIDRepository extends JpaRepository<ShortenerID, Long> {
}
