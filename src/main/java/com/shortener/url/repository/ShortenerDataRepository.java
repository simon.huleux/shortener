package com.shortener.url.repository;

import com.shortener.url.model.ShortenerData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ShortenerDataRepository extends JpaRepository<ShortenerData, Long> {
    Optional<ShortenerData> findByKey(String key);

    Optional<ShortenerData> findByUrl(String url);
}
