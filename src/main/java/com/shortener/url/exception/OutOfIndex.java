package com.shortener.url.exception;

public class OutOfIndex extends Exception {
    public OutOfIndex(String message) {
        super(message);
    }
}
