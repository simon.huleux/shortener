package com.shortener.url.controller;


import com.shortener.url.exception.OutOfIndex;
import com.shortener.url.model.ShortenerData;
import com.shortener.url.model.ShortenerRequest;
import com.shortener.url.service.ShortenerService;
import com.shortener.url.service.UrlValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

import static org.springframework.http.HttpStatus.*;

@RestController
public class ShortenerController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ShortenerController.class);

    private final UrlValidator urlValidator;
    private final ShortenerService shortenerService;

    @Autowired
    public ShortenerController(UrlValidator urlValidator, ShortenerService shortenerService) {
        this.urlValidator = urlValidator;
        this.shortenerService = shortenerService;
    }

    private String getCallerDomain(HttpServletRequest request) {
        ServletServerHttpRequest httpRequest = new ServletServerHttpRequest(request); //request is HttpServletRequest
        UriComponents uriComponents = UriComponentsBuilder.fromHttpRequest(httpRequest).build();
        String result = uriComponents.getScheme() + "://" + uriComponents.getHost();
        if (uriComponents.getPort() != -1) {

            result += ":" + uriComponents.getPort();

        }
        result += "/";
        return result;
    }

    @PostMapping(value = "/shortener", consumes = {"application/json"})
    public ResponseEntity<ShortenerRequest> shortenUrl(@RequestBody @Valid final ShortenerRequest shortenRequest) {
        LOGGER.info("Receive the url: {}", shortenRequest.getUrl());
        String rawUrl = shortenRequest.getUrl();
        if (urlValidator.isValid(rawUrl)) {
            String currentDomain = getCallerDomain(((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest());
            LOGGER.info("The caller call with the domain: {}", currentDomain);

            try {
                String id = this.shortenerService.save(rawUrl);
                LOGGER.info("Return the url: {}{}", currentDomain, id);
                ShortenerRequest response = new ShortenerRequest();
                response.setUrl(currentDomain + id);
                return new ResponseEntity<>(response, HttpStatus.CREATED);

            } catch (OutOfIndex e) {
                LOGGER.error("We reach the maximum of the id: {}", e.getMessage());
                throw new ResponseStatusException(EXPECTATION_FAILED, "We have an issue to process your request we are sorry.");
            }
        }
        LOGGER.info("We receive a no valid url: {}", rawUrl);
        throw new ResponseStatusException(BAD_REQUEST, "Your request are not a valid url.");

    }

    @GetMapping(value = "/{key}")
    public RedirectView getKey(@PathVariable String key) throws Exception {
        LOGGER.info("Receive the key: {}", key);

        Optional<ShortenerData> urlOp = this.shortenerService.getByKey(key);
        if (urlOp.isPresent()) {
            LOGGER.info("key founded: {}", key);

            RedirectView redirectView = new RedirectView();
            redirectView.setUrl(urlOp.get().getUrl());
            return redirectView;
        } else {
            LOGGER.info("key not found: {}", key);
            throw new ResponseStatusException(NOT_FOUND, "Unable to find by this key : " + key);
        }

    }

}

/*
SHA 512 solution
//            MessageDigest md = MessageDigest.getInstance("SHA-512");
//            byte[] messageDigest = md.digest(rawUrl.getBytes());
//            BigInteger no = new BigInteger(1, messageDigest);
//
//            String hashtext = no.toString(16);
//
//            while (hashtext.length() < 32) {
//                hashtext = "0" + hashtext;
//            }
//
//
//            String key = hashtext.substring(0, 10);
 */
