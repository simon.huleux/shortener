package com.shortener.url.service;

import com.shortener.url.exception.OutOfIndex;
import com.shortener.url.model.ShortenerData;
import com.shortener.url.model.ShortenerID;
import com.shortener.url.repository.ShortenerDataRepository;
import com.shortener.url.repository.ShortenerIDRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ShortenerService {
    public static final String INIT_KEY = Character.toString((char) 33);
    public static final int LAST_KEY = 126;
    private static final Logger LOGGER = LoggerFactory.getLogger(ShortenerService.class);
    private static final int MAX_CHAR = 10;

    private final ShortenerDataRepository shortenerDataRepository;
    private final ShortenerIDRepository shortenerIDRepository;

    @Autowired
    public ShortenerService(ShortenerDataRepository shortenerDataRepository, ShortenerIDRepository shortenerIDRepository) {
        this.shortenerDataRepository = shortenerDataRepository;
        this.shortenerIDRepository = shortenerIDRepository;
    }

    public Optional<ShortenerData> getByKey(String key) {
        return this.shortenerDataRepository.findByKey(key);
    }

    @Transactional
    public String save(String url) throws OutOfIndex {


        Optional<ShortenerData> shortenerDataOp = this.shortenerDataRepository.findByUrl(url);
        if (shortenerDataOp.isPresent()) {
            LOGGER.info("The url {} already exist for the key {}", url, shortenerDataOp.get().getKey());
            return shortenerDataOp.get().getKey();
        }
        LOGGER.info("The url {} not exist yet", url);

        Optional<ShortenerID> id = this.shortenerIDRepository.findAll().stream().findFirst();

        String key = generate(id.stream().map(ShortenerID::getKey).findFirst());

        ShortenerData save = new ShortenerData();
        save.setKey(key);
        save.setUrl(url);
        LOGGER.info("Save url {} with the key {}", url, key);

        this.shortenerDataRepository.save(save);


        if (id.isEmpty()) {
            LOGGER.info("The key is not saved yet {}", key);

            ShortenerID saveFirst = new ShortenerID();
            saveFirst.setKey(key);
            this.shortenerIDRepository.save(saveFirst);
        } else {
            ShortenerID update = id.get();
            update.setKey(key);
            LOGGER.info("The key is updated {}", key);

            this.shortenerIDRepository.save(update);
        }
        return key;
    }

    private String generate(Optional<String> id) throws OutOfIndex {
        if (id.isPresent()) {
            LOGGER.info("The key before increment {}", id.get());

            ArrayList<String> charValues = Arrays.stream(id.get().split("")).collect(Collectors.toCollection(ArrayList::new));
            List<String> charResult = new ArrayList<>();
            boolean quarry = false;

            if ((charValues.get(0).charAt(0)) + 1 > LAST_KEY) {
                charResult.add(INIT_KEY);
                quarry = true;
            } else {
                int e = charValues.get(0).charAt(0) + 1;

                e = verifyAvailableChar(e);


                charResult.add(Character.toString((char) e));
            }
            if (quarry && charValues.size() == 1) {
                charResult.add(INIT_KEY);
                return String.join("", charResult);
            }
            charValues.remove(0);
            if (quarry) {

                for (String cha : charValues) {
                    if ((cha.charAt(0)) + 1 > LAST_KEY) {

                        charResult.add(INIT_KEY);
                    } else {
                        quarry = false;
                        int e = cha.charAt(0) + 1;
                        e = verifyAvailableChar(e);

                        charResult.add(Character.toString((char) e));
                        break;

                    }
                }
                if (quarry) {
                    if (charResult.size() == MAX_CHAR) {
                        LOGGER.error("The key have reach his maximum");
                        throw new OutOfIndex("The index have reach is maximum");
                    }
                    charResult.add(INIT_KEY);

                }


            } else {
                charResult.addAll(charValues);
            }

            LOGGER.info("The key after increment {}", String.join("", charResult));

            return String.join("", charResult);
        } else {
            LOGGER.info("Return the init key");

            return INIT_KEY; // start init
        }
    }

    private int verifyAvailableChar(int e) {
        // " 34
        // # 35
        // % 37
        // ' 39
        // / 47
        // ? 63
        // \ 92
        // ^ 94
        // | 124
        List<Integer> notAvailable = Arrays.asList(34, 35, 37, 39, 47, 63, 92, 94, 124);
        while (notAvailable.contains(e)) {
            e++;
        }
        return e;
    }


}
