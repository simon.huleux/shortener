package com.shortener.url.service;

import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
public class UrlValidator {

    private static final String URL_REGEX = "^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$";
    private static final Pattern URL_PATTERN = Pattern.compile(URL_REGEX);

    public boolean isValid(String url) {
        return URL_PATTERN.matcher(url).matches();
    }
}
