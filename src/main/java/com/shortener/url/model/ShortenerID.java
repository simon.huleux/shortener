package com.shortener.url.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class ShortenerID {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;
    private String key;

}
