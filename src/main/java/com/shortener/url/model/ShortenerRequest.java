package com.shortener.url.model;

import lombok.Data;


@Data
public class ShortenerRequest {
    private String url;
}